package data;

import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import com.sun.mail.util.BASE64DecoderStream;
import com.sun.mail.util.BASE64EncoderStream;

public class Encrypt {
	private static Cipher ecipher;
	private static Cipher dcipher;
	private static SecretKey secretkey;

	public static void initEncrypt(SecretKey key) {
		try {
			// generate secret key using DES algorithm
			ecipher = Cipher.getInstance("DES");
			dcipher = Cipher.getInstance("DES");

			// initialize the ciphers with the given key
			ecipher.init(Cipher.ENCRYPT_MODE, key);
			dcipher.init(Cipher.DECRYPT_MODE, key);
		} catch (Exception e) {
			System.out.println("Create encrypt error" + e.getMessage());
		}
	}

	public static SecretKey genKey() {
		try {
			secretkey = KeyGenerator.getInstance("DES").generateKey();
			return secretkey;
		} catch (Exception e) {
			System.out.println("gen key error: " + e.getMessage());
		}
		return null;
	}

	public static String encrypt(String str) {
		try {
			// encode the string into a sequence of bytes using the named charset
			// storing the result into a new byte array.
			byte[] utf8 = str.getBytes("UTF8");
			byte[] enc = ecipher.doFinal(utf8);
			// encode to base64
			enc = BASE64EncoderStream.encode(enc);
			return new String(enc);
		} catch (Exception e) {
			System.out.println("encrypt error" + e.getMessage());
		}
		return null;
	}

	static String decrypt(String str) {
		try {
			// decode with base64 to get bytes
			byte[] dec = BASE64DecoderStream.decode(str.getBytes());
			byte[] utf8 = dcipher.doFinal(dec);
			// create new string based on the specified charset
			return new String(utf8, "UTF8");
		}

		catch (Exception e) {
			System.out.println("decrypt error" + e.getMessage());
		}
		return null;
	}
}
