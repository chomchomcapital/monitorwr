package data;

import display.View;
import java.awt.Color;
import java.awt.Component;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Properties;
import java.util.Vector;
import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class Data extends JTable {
	Vector<String> columnNames = new Vector();
	Vector<Vector<Object>> data = new Vector();
	DefaultTableModel tableModel;
	String querry;
	String[] userError;
	String currentDate;
	ResultSet rs;
	Connection con;
	Statement stmt;

	public Data() {
		this.tableModel = new DefaultTableModel(this.data, this.columnNames);
		this.currentDate = (new SimpleDateFormat("yyyyMMdd")).format(Calendar.getInstance().getTime());
		this.rs = null;
		this.con = null;
		this.stmt = null;
		System.out.println("Data constructor start");

		try {
			File fileInput = new File("./config/database.cfg");
			FileReader file = new FileReader(fileInput);
			BufferedReader reader = new BufferedReader(file);
			Properties prop = new Properties();
			prop.load(reader);
			String[] fields = prop.getProperty("fields").split(",");
			this.userError = prop.getProperty("usererror").split(",");
			this.columnNames.addAll(Arrays.asList(fields));
			this.setModel(this.tableModel);
			this.disable();
		} catch (Exception var6) {
			System.out.println("create data error: " + var6.getMessage());
		}

		System.out.println("Data constructor end");
	}

	public void setCellRender() {
		DefaultTableCellRenderer cellRender = new DefaultTableCellRenderer() {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
				super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
				this.setHorizontalAlignment(SwingConstants.CENTER);
				String status = table.getModel().getValueAt(row, 14).toString();
				boolean isWarning = false;
				String[] var12;
				int var11 = (var12 = Data.this.userError).length;

				for (int var10 = 0; var10 < var11; ++var10) {
					String error = var12[var10];
					if (status.equals(error)) {
						isWarning = true;
						break;
					}
				}

				if (status.equals("00")) {
					this.setBackground(Color.green);
				} else if (isWarning) {
					this.setBackground(Color.yellow);
				} else {
					this.setBackground(Color.red);
				}

				return this;
			}
		};
		this.setDefaultRenderer(Object.class, cellRender);
	}

	public synchronized void initData(Connection connect) throws SQLException {
		try {
			System.out.println("init data start");
			this.querry = " select * from (select rank() over(partition by 1 order by TRN_DT desc,RGS_TRN_TS desc,rowid desc) stt ,BK_CD ,MSG_ID_CD ,CD_TRCO_LI_CD ,CD_NO ,RCP_ACT_REF_TXT ,TRN_AM ,TRN_DT ,CD_MSG_CHSE_NO ,TRN_TM ,MCH_TPCD ,DL_IST_CD ,ISU_BK_CD ,RCP_BK_CD ,CD_TRN_RSP_CD from vch24006tf where (TBK_PRC_ID = 'ISS' or TBK_PRC_ID= 'ACQ' or TBK_PRC_ID='BNB')  and  TRN_DT like '" + this.currentDate + "' and DL_IST_CD like '%' and CD_NO like '%'" + " and (('%' <>'%' and RCP_BK_CD like '%' || '%' || '%') or '%' ='%') " + " and CD_TRCO_LI_CD like '%' and CD_MSG_CHSE_NO like '%' and CD_NO like '%' " + " and BK_CD = 'W5970' and (('%' <>'%' and MCH_TPCD like '%' || '%' || '%') or '%' ='%') ) a where a.stt<=140";
			this.stmt = connect.createStatement();
			long startTimeVch24006tf = System.currentTimeMillis();
			this.rs = this.stmt.executeQuery(this.querry);
			long endTimeVch24006tf = System.currentTimeMillis();
			System.out.println("init data in: " + (endTimeVch24006tf - startTimeVch24006tf));
			this.data.clear();

			while (this.rs.next()) {
				String[] arrayValues = new String[]{this.rs.getString("STT"), this.rs.getString("BK_CD"), this.rs.getString("MSG_ID_CD"), this.rs.getString("CD_TRCO_LI_CD"), this.rs.getString("CD_NO"), this.rs.getString("RCP_ACT_REF_TXT"), this.rs.getString("TRN_AM"), this.rs.getString("TRN_DT"), this.rs.getString("CD_MSG_CHSE_NO"), this.rs.getString("TRN_TM"), this.rs.getString("MCH_TPCD"), this.rs.getString("DL_IST_CD"), this.rs.getString("ISU_BK_CD"), this.rs.getString("RCP_BK_CD"), this.rs.getString("CD_TRN_RSP_CD")};
				Vector<Object> values = new Vector();
				values.addAll(Arrays.asList(arrayValues));
				this.data.add(values);
			}
		} catch (Exception var17) {
			System.out.println("get data error: " + var17);
			int input = JOptionPane.showOptionDialog((Component) null, "get data error: " + var17.getMessage(), "Get Data", -1, 0, (Icon) null, (Object[]) null, (Object) null);
			if (input == 0) {
				System.exit(3);
			}
		} finally {
			try {
				this.rs.close();
				this.rs = null;
				this.stmt.close();
				this.stmt = null;
			} catch (SQLException var16) {
				System.out.println("close data error: " + var16);
				int input = JOptionPane.showOptionDialog((Component) null, "close data error: " + var16.getMessage(), "Close Data", -1, 0, (Icon) null, (Object[]) null, (Object) null);
				if (input == 0) {
					System.exit(3);
				}
			}

		}

		System.out.println("init data end");
	}

	public synchronized void updateData(Connection connect) throws SQLException {
		System.out.println("update data start");
		boolean isSearch = false;
		try {
			String date;
			String acq = "%";
			String cardNumber1 = "%";
			String bnb = "%";
			String pCode = "%";
			String trace = "%";
			String cardNumber2 = "%";
			String mcc = "%";
			this.stmt = connect.createStatement();
			if (View.dateTextInput.getText().equals("")) {
				date = this.currentDate;
			} else {
				date = View.dateTextInput.getText().trim();
				isSearch=true;
			}

			if (View.acqListBox.getSelectedItem().equals("")) {
				acq = "%";
			} else {
				acq = View.acqListBox.getSelectedItem().toString().trim().substring(0, 6);
				isSearch=true;
			}

			if (View.issListBox.getSelectedItem().equals("")) {
				cardNumber1 = "%";
			} else {
				cardNumber1 = View.issListBox.getSelectedItem().toString().trim().substring(0, 6) + "%";
				isSearch=true;
			}

			if (View.bnbListBox.getSelectedItem().equals("")) {
				bnb = "%";
			} else {
				bnb = View.bnbListBox.getSelectedItem().toString().trim().substring(0, 6);
				isSearch=true;
			}

			if (View.traceTextInput.getText().equals("")) {
				trace = "%";
			} else {
				trace = View.traceTextInput.getText().trim();
				isSearch=true;
			}

			if (View.panTextInput.getText().equals("")) {
				cardNumber2 = "%";
			} else {
				cardNumber2 = View.panTextInput.getText().trim();
				isSearch=true;
			}

			if (View.typeListBox.getSelectedItem().equals("All")) {
				pCode = "%";
				mcc = "%";
				
			} else if (View.typeListBox.getSelectedItem().equals("ATM")) {
				pCode = "%";
				mcc = "6011";
				isSearch=true;
			} else if (View.typeListBox.getSelectedItem().equals("POS")) {
				pCode = "00__00";
				mcc = "%";
				isSearch=true;
			} else if (View.typeListBox.getSelectedItem().equals("IBFT INQ")) {
				pCode = "43____";
				mcc = "%";
				isSearch=true;
			} else if (View.typeListBox.getSelectedItem().equals("IBFT")) {
				pCode = "91____";
				mcc = "%";
				isSearch=true;
			} else {
				pCode = "%";
				mcc = "%";
				
			}

			if (isSearch) {
				this.querry = " select * from (select rank() over(partition by 1 order by TRN_DT desc,RGS_TRN_TS desc,rowid desc) stt ,BK_CD ,MSG_ID_CD ,CD_TRCO_LI_CD ,CD_NO ,RCP_ACT_REF_TXT ,TRN_AM ,TRN_DT ,CD_MSG_CHSE_NO ,TRN_TM ,MCH_TPCD ,DL_IST_CD ,ISU_BK_CD ,RCP_BK_CD ,CD_TRN_RSP_CD from vch24006tf where (TBK_PRC_ID = 'ISS' or TBK_PRC_ID= 'ACQ' or TBK_PRC_ID='BNB') and TRN_DT like '" + date + "' and DL_IST_CD like '" + acq + "' and CD_NO like '" + cardNumber1 + "'" + " and (('" + bnb + "' <>'%' and RCP_BK_CD like '%' || '" + bnb + "' || '%') or '" + bnb + "' ='%')" + " and CD_TRCO_LI_CD like '" + pCode + "' and CD_MSG_CHSE_NO like '" + trace + "' and CD_NO like '" + cardNumber2 + "'  and BK_CD = 'W5970'" + " and (('" + mcc + "' <>'%' and MCH_TPCD like '%' || '" + mcc + "' || '%') or '" + mcc + "' ='%') ) a where a.stt<=140";
			} else {
				this.querry = " select rownum as STT, u.* from( select BK_CD , MSG_ID_CD , CD_TRCO_LI_CD , CD_NO , RCP_ACT_REF_TXT , TRN_AM , TRN_DT , CD_MSG_CHSE_NO , TRN_TM , MCH_TPCD , DL_IST_CD , ISU_BK_CD , RCP_BK_CD , CD_TRN_RSP_CD from vch24006tf where BK_CD = 'W5970' and TRN_DT = to_char(sysdate,'yyyyMMdd') and TRN_TM >= to_char(sysdate - 1/128,'hh24miss') order by TRN_TM,rownum desc) u where rownum <=140 ;";
			}

			long startTime = System.currentTimeMillis();
			this.rs = this.stmt.executeQuery(this.querry);
			long endTime = System.currentTimeMillis();
			this.data.clear();

			while (this.rs.next()) {
				String[] arrayValues = new String[]{this.rs.getString("STT"), this.rs.getString("BK_CD"), this.rs.getString("MSG_ID_CD"), this.rs.getString("CD_TRCO_LI_CD"), this.rs.getString("CD_NO"), this.rs.getString("RCP_ACT_REF_TXT"), this.rs.getString("TRN_AM"), this.rs.getString("TRN_DT"), this.rs.getString("CD_MSG_CHSE_NO"), this.rs.getString("TRN_TM"), this.rs.getString("MCH_TPCD"), this.rs.getString("DL_IST_CD"), this.rs.getString("ISU_BK_CD"), this.rs.getString("RCP_BK_CD"), this.rs.getString("CD_TRN_RSP_CD")};
				Vector<Object> values = new Vector();
				values.addAll(Arrays.asList(arrayValues));
				this.data.add(values);
			}

			System.out.println("udpate data in: " + (endTime - startTime));
			this.tableModel.fireTableDataChanged();
		} catch (SQLException var25) {
			System.out.println("udpate data error: " + var25);
			int input = JOptionPane.showOptionDialog((Component) null, "udpate data error: " + var25.getMessage(), "Update Data", -1, 0, (Icon) null, (Object[]) null, (Object) null);
			if (input == 0) {
				System.exit(3);
			}
		} finally {
			try {
				this.rs.close();
				this.rs = null;
				this.stmt.close();
				this.stmt = null;
			} catch (SQLException var24) {
				System.out.println("close data error: " + var24);
				int input = JOptionPane.showOptionDialog((Component) null, "close data error: " + var24.getMessage(), "Close Connect", -1, 0, (Icon) null, (Object[]) null, (Object) null);
				if (input == 0) {
					System.exit(3);
				}
			}

		}

		System.out.println("update data end");
	}
}
