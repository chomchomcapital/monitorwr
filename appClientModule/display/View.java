package display;

import java.awt.Color;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane.RestoreAction;
import javax.swing.text.DocumentFilter;

import java.text.SimpleDateFormat;

import data.Data;

public class View extends JFrame {

	public static boolean isRunning = true;
	public static boolean isRealTime = true;

	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	int byWidth = screenSize.width / 100;
	int byHeight = screenSize.height / 100;
	int scrollWidth = 1;
	int scrollHeight = 1;
	int FrameWidth = 1;
	int FrameHeight = 1;
	JButton pauseButton;
	JButton searchButton;
	JLabel acqListLabel;
	public static JComboBox<String> acqListBox;
	JLabel issListLabel;
	public static JComboBox<String> issListBox;
	JLabel bnbListLabel;
	public static JComboBox<String> bnbListBox;
	JLabel typeLabel;
	public static JComboBox<String> typeListBox;
	JLabel traceLabel;
	public static JTextField traceTextInput;
	JLabel panLabel;
	public static JTextField panTextInput;
	JLabel dateLabel;
	public static JTextField dateTextInput;
	public static JCheckBox realTimeCheckBox;
	Data tableData;
	JScrollPane scrollViewData;

	public View() {
		// TODO Auto-generated constructor stub
		super("Monitor Application");
		System.out.println("View constructor start");
		try {
			setLayout(null);
			setSize(this.getToolkit().getScreenSize());// full screen
//			setExtendedState(JFrame.MAXIMIZED_BOTH);// full screen
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// exit on close
			Image image = Toolkit.getDefaultToolkit().createImage("./config/image.jpg");
			setIconImage(image);
//			getContentPane().setBackground(new java.awt.Color(250, 250, 250));
			getContentPane().setBackground(new java.awt.Color(217, 231, 245));
			getRootPane().setWindowDecorationStyle(JRootPane.FRAME);
			setUndecorated(true);
			setResizable(false);
			System.out.println(byWidth + "-" + byHeight);
		} catch (Exception e) {
			System.out.println("View constructor: " + e.getMessage());
			int input = JOptionPane.showOptionDialog(null, "Create view error: " + e.getMessage(), "Create View",
					JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE, null, null, null);
			if (input == JOptionPane.OK_OPTION) {
				System.exit(EXIT_ON_CLOSE);
			}
		}
		System.out.println("View constructor end");
	}

	public void renderComponent(Connection connect) {
		System.out.println("View render component start");
		try {

			System.out.println("prepare pause button");
			// pause button
			this.pauseButton = new JButton("Pause");
			this.pauseButton.setBounds(byWidth * 90, byHeight, byWidth * 5, byHeight * 3);
			this.pauseButton.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					pauseButton.setText(isRunning ? "Resume" : "Pause");
					isRunning = !isRunning;
				}
			});

			// read properties from config file
			System.out.println("prepare properties file");
//			File fileInput = new File(Main.class.getResource("/config/database.cfg").toURI());
			File fileInput = new File("./config/database.cfg");
			FileReader file = new FileReader(fileInput);
			BufferedReader reader = new BufferedReader(file);
			Properties prop = new Properties();
			prop.load(reader);
			String[] acqList = prop.getProperty("acqlist").split(",");
			String[] issList = prop.getProperty("isslist").split(",");
			String[] bnbList = prop.getProperty("bnblist").split(",");
			this.scrollWidth = Integer.parseInt(prop.getProperty("scrollwidth"));
			this.scrollHeight = Integer.parseInt(prop.getProperty("scrollheight"));

			// label style
//			Border border = BorderFactory.createLineBorder(Color.black, 1);
			Border border = BorderFactory.createLineBorder(new java.awt.Color(88, 128, 161), 1);
			Color labelColor = new java.awt.Color(223, 240, 254);
			Font labelFont = new Font("Arial", Font.BOLD, 13);

			System.out.println("prepare acq list");
			// acquirer label
			this.acqListLabel = new JLabel("ACQ List");
			this.acqListLabel.setFont(labelFont);
			this.acqListLabel.setHorizontalAlignment(SwingConstants.CENTER);
			this.acqListLabel.setOpaque(true);
			this.acqListLabel.setBackground(labelColor);
			this.acqListLabel.setBorder(border);
			this.acqListLabel.setForeground(new java.awt.Color(5, 66, 117));
			this.acqListLabel.setBounds(byWidth * 2, byHeight, 70, 30);
			// acq list combobox
			acqListBox = new JComboBox<String>(acqList);
			acqListBox.insertItemAt("", 0);
			acqListBox.setSelectedIndex(0);
			acqListBox.setFont(new Font("Arial", Font.BOLD, 15));
			acqListBox.setBounds(byWidth * 7, byHeight, 150, 30);

			System.out.println("prepare iss list");
			// issuer label
			this.issListLabel = new JLabel("ISS List");
			this.issListLabel.setFont(labelFont);
			this.issListLabel.setHorizontalAlignment(SwingConstants.CENTER);
			this.issListLabel.setOpaque(true);
			this.issListLabel.setBackground(labelColor);
			this.issListLabel.setBorder(border);
			this.issListLabel.setForeground(new java.awt.Color(5, 66, 117));
			this.issListLabel.setBounds(byWidth * 20, 10, 70, 30);
			// iss list combobox
			issListBox = new JComboBox<String>(issList);
			issListBox.insertItemAt("", 0);
			issListBox.setSelectedIndex(0);
			issListBox.setFont(new Font("Arial", Font.BOLD, 15));
			issListBox.setBounds(byWidth * 25, 10, 150, 30);

			System.out.println("prepare bnb list");
			// beneficiary label
			this.bnbListLabel = new JLabel("BNB List");
			this.bnbListLabel.setFont(labelFont);
			this.bnbListLabel.setHorizontalAlignment(SwingConstants.CENTER);
			this.bnbListLabel.setOpaque(true);
			this.bnbListLabel.setBackground(labelColor);
			this.bnbListLabel.setBorder(border);
			this.bnbListLabel.setForeground(new java.awt.Color(5, 66, 117));
			this.bnbListLabel.setBounds(byWidth * 38, 10, 70, 30);
			// bnb list combobox
			bnbListBox = new JComboBox<String>(bnbList);
			bnbListBox.insertItemAt("", 0);
			bnbListBox.setSelectedIndex(0);
			bnbListBox.setFont(new Font("Arial", Font.BOLD, 15));
			bnbListBox.setBounds(byWidth * 43, 10, 150, 30);

			System.out.println("prepare combo box");
			// type label
			this.typeLabel = new JLabel("Type");
			this.typeLabel.setFont(labelFont);
			this.typeLabel.setHorizontalAlignment(SwingConstants.CENTER);
			this.typeLabel.setOpaque(true);
			this.typeLabel.setBackground(labelColor);
			this.typeLabel.setBorder(border);
			this.typeLabel.setForeground(new java.awt.Color(5, 66, 117));
			this.typeLabel.setBounds(byWidth * 56, 10, 70, 30);
			// type list combobox
			String[] typeList = { "All", "ATM", "POS", "IBFT INQ", "IBFT" };
			typeListBox = new JComboBox<String>(typeList);
			typeListBox.setFont(new Font("Arial", Font.BOLD, 15));
			typeListBox.setBounds(byWidth * 61, 10, 100, 30);

			System.out.println("prepare trace input");
			// trace label
			this.traceLabel = new JLabel("Trace");
			this.traceLabel.setFont(labelFont);
			this.traceLabel.setHorizontalAlignment(SwingConstants.CENTER);
			this.traceLabel.setOpaque(true);
			this.traceLabel.setBackground(labelColor);
			this.traceLabel.setBorder(border);
			this.traceLabel.setForeground(new java.awt.Color(5, 66, 117));
			this.traceLabel.setBounds(byWidth * 2, 50, 70, 30);
			// trace text input
			traceTextInput = new JTextField();
			traceTextInput.setFont(new Font("Arial", Font.BOLD, 15));
			traceTextInput.setBounds(byWidth * 7, 50, 80, 30);
			traceTextInput.setBackground(new java.awt.Color(255, 255, 250));
			traceTextInput.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					// TODO Auto-generated method stub
					super.keyTyped(e);
					if (Character.isDigit(e.getKeyChar())) {
						if (traceTextInput.getText().length() >= 6) {
							e.consume();
						}
					} else {
						e.consume();
					}
				}
			});

			System.out.println("prepare pan input");
			// pan label
			this.panLabel = new JLabel("PAN");
			this.panLabel.setFont(labelFont);
			this.panLabel.setHorizontalAlignment(SwingConstants.CENTER);
			this.panLabel.setOpaque(true);
			this.panLabel.setBackground(labelColor);
			this.panLabel.setBorder(border);
			this.panLabel.setForeground(new java.awt.Color(5, 66, 117));
			this.panLabel.setBounds(byWidth * 20, 50, 70, 30);
			// pan text input
			panTextInput = new JTextField();
			panTextInput.setFont(new Font("Arial", Font.BOLD, 15));
			panTextInput.setBounds(byWidth * 25, 50, 150, 30);
			panTextInput.setBackground(new java.awt.Color(255, 255, 250));
			panTextInput.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					// TODO Auto-generated method stub
					super.keyTyped(e);
					if (Character.isDigit(e.getKeyChar())) {
						if (panTextInput.getText().length() >= 19) {
							e.consume();
						}
					} else {
						e.consume();
					}
				}
			});

			System.out.println("prepare date input");
			// date label
			this.dateLabel = new JLabel("Date");
			this.dateLabel.setFont(labelFont);
			this.dateLabel.setHorizontalAlignment(SwingConstants.CENTER);
			this.dateLabel.setOpaque(true);
			this.dateLabel.setBackground(labelColor);
			this.dateLabel.setBorder(border);
			this.dateLabel.setForeground(new java.awt.Color(5, 66, 117));
			this.dateLabel.setBounds(byWidth * 38, 50, 70, 30);
			// date text input
			dateTextInput = new JTextField();
			dateTextInput.setFont(new Font("Arial", Font.BOLD, 13));
			dateTextInput.setBounds(byWidth * 43, 50, 80, 30);
			dateTextInput.setBackground(new java.awt.Color(255, 255, 250));
			dateTextInput.setText(new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime()));
			dateTextInput.disable();
			dateTextInput.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					// TODO Auto-generated method stub
					super.keyTyped(e);
					if (Character.isDigit(e.getKeyChar())) {
						if (dateTextInput.getText().length() >= 8) {
							e.consume();
						}
					} else {
						e.consume();
					}
				}
			});

			System.out.println("prepare real time check box");
			// real time check box
			realTimeCheckBox = new JCheckBox("Real Time Monitor");
			realTimeCheckBox.setBounds(byWidth * 90, 50, 150, 30);
			realTimeCheckBox.setSelected(true);
			realTimeCheckBox.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent e) {
					isRealTime = realTimeCheckBox.isSelected();
					System.out.println(isRealTime);
					if (isRealTime) {
						dateTextInput.disable();
						dateTextInput.repaint();
						dateTextInput
								.setText(new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime()));
					} else {
						dateTextInput.enable();
						dateTextInput.repaint();
					}
				}
			});

			System.out.println("prepare data");
			// data scroll view
			try {
				this.tableData = new Data();
				this.tableData.setCellRender();
				this.tableData.initData(connect);
				this.scrollViewData = new JScrollPane(tableData);
				this.scrollViewData.setBounds(0, byHeight * 10, screenSize.width * scrollWidth,
						byHeight * scrollHeight);
				this.scrollViewData.setBackground(Color.GRAY);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				int input = JOptionPane.showOptionDialog(null, "data error: " + e.getMessage(), "View Render",
						JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE, null, null, null);
				if (input == JOptionPane.OK_OPTION) {
					System.exit(EXIT_ON_CLOSE);
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
			int input = JOptionPane.showOptionDialog(null, "render error: " + e.getMessage(), "View Render",
					JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE, null, null, null);
			if (input == JOptionPane.OK_OPTION) {
				System.exit(EXIT_ON_CLOSE);
			}
		}
		System.out.println("View render component end");
	}

	public void showComponent() {
		System.out.println("View show component start");
		add(this.scrollViewData);
		add(this.pauseButton);
//		add(this.searchButton);
		add(this.acqListLabel);
		add(acqListBox);
		add(this.issListLabel);
		add(issListBox);
		add(this.bnbListLabel);
		add(bnbListBox);
		add(this.typeLabel);
		add(typeListBox);
		add(this.traceLabel);
		add(traceTextInput);
		add(this.panLabel);
		add(panTextInput);
		add(realTimeCheckBox);
		add(this.dateLabel);
		add(dateTextInput);
		setVisible(true);
		System.out.println("View show component end");
	}
}
