package display;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import javax.swing.*;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

public class Main {

	public static void main(String[] args) throws SQLException {

		System.out.println("Start application ");
		Connection connect = null;
		String user = null;
		String password = null;
		String host= null;
		String port = null;
		String sid = null;

		try {
		File fileInput = new File("./config/database.cfg");
		FileReader file = new FileReader(fileInput);
		BufferedReader reader = new BufferedReader(file);
		Properties prop = new Properties();
		prop.load(reader);

		user = prop.getProperty("user");
		password = prop.getProperty("password");
		host = prop.getProperty("host");
		port = prop.getProperty("port");
		sid = prop.getProperty("sid");

		System.out.println("init data start");
		System.out.println("init user:" + user);
		System.out.println("init password:" + password);
		System.out.println("init host:" + host);
		System.out.println("init port:" + port);
		System.out.println("init sid:" + sid);

		connect = DriverManager.getConnection("jdbc:oracle:thin:@" + host + ":" + port + "/" + sid,
				user, password);

		}catch(Exception ex) {
			System.out.println(ex.getMessage());
			int input = JOptionPane.showOptionDialog(null, "main create connection error: " + ex.getMessage(), "Main",
					JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE, null, null, null);
			if (input == JOptionPane.OK_OPTION) {
				System.exit(JFrame.EXIT_ON_CLOSE);
			}
		}

		View frame = new View();
		frame.renderComponent(connect);
		frame.showComponent();

		while (true) {
			frame.setAutoRequestFocus(false); // ko nhap nhay nua nhe
			if (View.isRunning) {
				frame.tableData.updateData(connect);
			}
			if(View.isRealTime) {
				View.dateTextInput.setText(new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime()));
			}
			try {
				Thread.sleep(2000);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				int input = JOptionPane.showOptionDialog(null, "main error: " + e.getMessage(), "Main",
						JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE, null, null, null);
				if (input == JOptionPane.OK_OPTION) {
					System.exit(JFrame.EXIT_ON_CLOSE);
				}
			}
		}

	}

}
